<?php

namespace App;


define("BASIS","Bangladesh Association of Software & Information Services");

const BITM= BASIS . "Instite of Technology &Management";
echo BITM;

echo"<hr>";

echo __LINE__."<br>";
echo __FILE__."<br>";
echo __DIR__."<br>";

echo basename(__FILE__)."<br>";

function doSomething(){
    echo "I'm inside".__FUNCTION__."function<br>";
}

function doingSomething(){
    echo "I'm inside".__FUNCTION__."function<br>";
}

trait MyTrait{
    public function writingAMethodFromTrait(){
        echo "I'm inside".__TRAIT__."Trait<br>";
    }

}

class MyClass{
    use MyTrait;
    public function writingAMethodFromClass(){

        echo "I'm inside".__CLASS__."Class<br>";
        echo "I'm inside".__METHOD__."Method<br>";
    }
}


$myObj = new Myclass();
$myObj->writingAMethodFromTrait();

echo "I'm inside".__NAMESPACE__."Namespace<br>";